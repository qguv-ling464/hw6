#!/usr/bin/env python3

with open("wsj.txt", 'r') as wsj_file:
    # parse the WSJ corpus into a list of sentences
    # get rid of inverted sentences because they mess up relclauses
    snt_gap = "\n( (S "
    heretic_sinv = "\n( (SINV "
    sinv_polluted = wsj_file.read().split(heretic_sinv) # split at SINV
    wsj = []
    for pol in sinv_polluted:
        wsj.extend(pol.split(snt_gap)[1:]) # throw away first (SINV) match

a = b = c = d = 0
for snt in wsj:

    # ignore sentences without RCs
    if "WHNP" not in snt: continue

    # get all RCs in the sentence and split each into a tuple
    # zip is necessary because some pieces do double-duty
    # ... before1 WHNP1 (after1 == before2) WHNP2 after2

    rc_pieces = snt.split("WHNP")
    rcs = zip(rc_pieces[:-1], rc_pieces[1:])

    # for each RC
    for before, after in rcs:

        # grab the name of the WHNP head (from context)
        head = after.split(") )\n")[0].split(" ")[-1]

        # headless clauses have no head; WSJ calls this '0'
        null_head = head == "0"

        # treating it as a fair assumption that all WHNP relclauses modify NPs
        a += 1

        # if it's an object NP
        if "(VP" in before:
            b += 1
            if null_head: d += 1

        # if it's a subject NP
        elif null_head:
            c += 1

print("WSJ:")
print(" a = ~{}".format(a))
print(" b <  ", b)
print(" c >   ", c)
print(" d <  ", d)
