#!/usr/bin/env python3
from re import compile

# matching group #1 matches a tag of the heads that indicate a relclause,
# surrounded by optional sub-tags (as is the Brown way) and the rest of the
# sentence for context

# matches any relclause head tag
head_tags = r'cs|wp\$|wdt|wpo|wps|wql|wrb'

# matches any sentence which includes a relclause head tag
match_rc_sentences = compile(r'^.*/\S*(' + head_tags + ')\S*.*$')

# Same as above, but works only after a tag string is formed from the original
# sentence. Includes matching groups such that:
#  - group 1 is the text from the beginning of the sentence to the head tag
#  - group 2 is the head tag
#  - group 3 is the text from the head tag to the end of the sentence
match_head_tag = compile(r'^(.*) \S*(' + head_tags + ')\S* (.*)$')

def pull_tags(sentence) -> "sentence of tags":
    '''Given a Brown-formatted sentence, give a "tag string" with just the tags
    from the original sentence.'''

    return ' '.join([ word.split('/')[1] for word in sentence.split(' ') ])

def pull_relclause_sentences(brown) -> ["sentences with relclauses"]:
    '''Given a Brown-formatted corpus, spit out all sentences with at least one
    relative clause.'''

    return [ line.strip() for line in brown
            if match_rc_sentences.search(line) is not None ]

def get_relclause_tags(brown):
    '''Given a Brown-formatted corpus, spit out (a list of) lists of tags
    representing the tags of all sentences with relclauses.'''

    return list(map(pull_tags, pull_relclause_sentences(brown)))

def brown_questions(brown: "corpus"):
    '''Prints results to questions A-D from the Brown corpus.'''

    sentences = get_relclause_tags(brown)
    a, b = 0, 0 # the only questions we can answer

    for sentence in sentences:
        matches = match_head_tag.finditer(sentence)

        # ignore sentences without relclauses
        if matches is None: continue

        for match in matches:
            before, _, after = match.groups()

            # turn match groups into lists of tags
            before, after = before.split(' '), after.split(' ')

            # To find the number of relative clauses modifying NPs, as in "The
            # guy *that you met* is an old friend of mine," we test if the word
            # immediately before the head is a noun. This should give us
            # relclauses modifying NPs, but will sometimes give
            # false-negatives.
            if 'nn' in before[-1]:
                a += 1

                # To find the number of relative clauses modifying an object
                # NP, as in "The guy hates the author *that I was telling you
                # about,*" we'll test if there's a verb before the match in the
                # sentence. This should approximate whether the noun is in
                # object position. This would not include sentences like "The
                # guy *that I was telling you about* hates the author."
                if any([w.startswith('vb')
                    or w.startswith('hv')
                    or w.startswith('be')
                    for w in before]):
                    b += 1

    return (a, b)

with open("brown.txt", 'r') as brownCorpus:
    a, b = brown_questions(brownCorpus)

print("Brown Corpus:")
print(" a >", a)
print(" b <", b)
print(" c    N/A")
print(" d    N/A")
